package main

import (
	"reflect"
	"testing"
  "net/http"
  "strconv"
  "net/http/httptest"
  "regexp"
)

// testverbs/nouns/sep.txt all have 3 lines and
// should remain the same.

func TestReadLines(t *testing.T) {

	t.Run("testfiles/test with verbs", func(t *testing.T) {
	got := readLines("testfiles/testverbs.txt")
	want := []string{"Running", "Running", "Running"}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
		}
})

	t.Run("testfiles/test with nouns", func(t *testing.T) {
	got := readLines("testfiles/testnouns.txt")
	want := []string{"Turtle", "Turtle", "Turtle"}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
		}
})

	t.Run("testfiles/test with separator", func(t *testing.T) {
	got := readLines("testfiles/testsep.txt")
	want := []string{"*", "*", "*"}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
		}
})

}

func TestLineCount(t *testing.T) {
	s := 	readLines("testfiles/testverbs.txt")
	got := lineCount(s)
	want := 3
	if got != want {
		t.Errorf("got %v want %v", got, want)
	}
}

func TestRandomNumber(t *testing.T) {
	t.Run("testfiles/test with two fixed numbers", func(t *testing.T) {
		got := randomNumber(1, 1)
		want := 1
		if got != want {
			t.Errorf("got %v want %v", got, want)
		}
	})

	t.Run("testfiles/test with lines from 3 line file", func(t *testing.T) {
	s := 	readLines("testfiles/testsep.txt")
		got := randomNumber(lineCount(s), lineCount(s))
		want := 3
		if got != want {
			t.Errorf("got %v want %v", got, want)
		}
	})
}

func TestPickWord(t *testing.T) {
  		s := make([]string, 3)
		s[0] = "testfiles/test"
		s[1] = "testfiles/test"
		s[2] = "testfiles/test"
	got := pickWord(s)
	want := "testfiles/test"
	if got != want {
		t.Errorf("got %v want %v", got, want)
		}
	}

func TestAssemblePassphrase(t *testing.T) {
	verbs := 	readLines("testfiles/testverbs.txt")
	nouns := 	readLines("testfiles/testnouns.txt")
	sep := 	readLines("testfiles/testsep.txt")
  ri := strconv.Itoa(randomNumber(5,5))

	got := assemblePassphrase(verbs, nouns, sep, ri)
	want := "Running*Turtle*5"
	if got != want {
		t.Errorf("got %v want %v", got, want)
		}
	}

// test the generated password is ouput the the web page
func TestWebServer(t *testing.T) {

  // Create a request to pass to our handler. We don't have any query parameters for now, so we'll
  // pass 'nil' as the third parameter.
    req, err := http.NewRequest("GET", "/health-check", nil)
    if err != nil {
        t.Fatal(err)
    }
    // We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
    rr := httptest.NewRecorder()
    handler := http.HandlerFunc(webServer)
    // Our handlers satisfy http.Handler, so we can call their ServeHTTP method 
    // directly and pass in our Request and ResponseRecorder.
    handler.ServeHTTP(rr, req)
    // Check the status code is what we expect.
    if status := rr.Code; status != http.StatusOK {
        t.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusOK)
    }
    stringval := rr.Body.String()

    // not sure where this goes yet
  	t.Run("Test if the regex pattern matches", func(t *testing.T) {
       r, _ := regexp.Compile("[A-Z].*[\\*\\&\\^\\%\\$\\#\\@\\!][A-Z].*\\D")
    // Check the response body is what we expect.
    want := true
    if r.MatchString(stringval) != want {
        t.Errorf("handler returned unnxpected body of \"%v\": got %v want %v",
           stringval, r.MatchString(rr.Body.String()), want)
    }
  })

    	t.Run("testfiles/test with two fixed numbers", func(t *testing.T) {
    // Check the response body is what we expect.
    got := len(stringval)
    want := 14
    if got <= want {
        t.Errorf("handler returned unexpected body: got %v want %v",
            got, want)
    }
  })

}

