# Passphrase Generator

**This app is currently live at [https://passgen.jptech.app/](https://passgen.jptech.app/)**

I am making this passphrase generator to replace [this
website](https://www.worksighted.com/random-passphrase-generator/#passphrase-generator)
that I have been using to generate customer passphrases, but it has a lot of
unfriendly combinations I have see (like `Angry*Children&5`), it also is random
on it's separators, and I want to keep them the same to it is easy for the
client to remember. I generally look for 14 characters, with the same separator
and a number at the end. The formula I want to have is `Verb*Noun*Number`. So, I
figured this would be a good go project.

The plan is to make this a lambda function, so I don't need a full-on server to
generate the codes. Eventually, I would like to roll it into a little password
sharing tool in golang that I have been using.

Eventually, I will populate the text files with nouns and verbs that should be
happy or at least not negative.

## Testing

There are 3 test files, these all have 3 of the same lines each. I am keeping
that to exactly 3 and the same strings because it is the only way I can think
of to use a random number generator but also have the results be predictable.



