# syntax=docker/dockerfile:1

FROM golang:1.17-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./

RUN go mod download

COPY *.go ./
COPY *.txt ./

RUN go build -o ./passgen

EXPOSE 8080

CMD [ "./passgen" ]

