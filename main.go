package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"time"
  "strconv"
  "net/http"
)

// pupulate vars
var verbs = readLines("verbs.txt")
var nouns = readLines("nouns.txt")
var sep = readLines("sep.txt")


func main() {

// create the web handler
  http.HandleFunc("/", webServer)
  http.ListenAndServe(":8080", nil)
}

// create the http server
func webServer(w http.ResponseWriter, r *http.Request) {
  passphrase := assemblePassphrase (verbs, nouns, sep, strconv.Itoa(randomNumber(0,9)))
 if stringLength(passphrase) > 14 {
  fmt.Fprintf(w, "%s\n", passphrase)
  // if you want this printed to the screen uncommend 
  // fmt.Printf("%s\n", passphrase)
} else {
  oops := "Oops... the passphrase \"" + passphrase + "\" is too short. Please try again"
    fmt.Fprintf(w, "%s\n", oops)
  }
}

// find out how many lines
func lineCount(input []string) int {
	return len(input)
}

// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string) {
	file, _ := os.Open(path)
	defer file.Close()
	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines
}

// generate a random number between two ints, return int
func randomNumber(min int, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min+1) + min
}

// generate a random word from slice, return string
func pickWord(input []string) string {
  min := 0
  max := len(input) - 1
  word := rand.Intn(max-min+1) + min
  return input[word]
}

// assemble the passphrase
func assemblePassphrase(v []string, n []string, s []string, i string) string {
  rv := pickWord(v)
  rn := pickWord(n)
  rs := pickWord(s)
  result := rv+rs+rn+rs+i
    return  result
  }

// check string length
func stringLength(s string) int {
  return len(s)
}
